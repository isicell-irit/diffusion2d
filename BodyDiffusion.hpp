#ifndef ONKO3D_3_0_BODYDIFFUSION_HPP
#define ONKO3D_3_0_BODYDIFFUSION_HPP

/**
 * @file BodyDiffusion.hpp
 * @brief Defines the BodyDiffusion class for managing molecule diffusion in a cell body.
 */

#include "DiffusionGrid.hpp"
#include "../../../../src/core/BaseBody.hpp"
#include <vector>
#include <hash_map>

using namespace std;

/**
 * @namespace Diffusion2D
 * @brief Namespace for 2D diffusion-related classes and functions.
 */
namespace Diffusion2D {

    /**
     * @class BodyDiffusion
     * @brief Class for managing molecule diffusion in a cell body.
     * 
     * @tparam cell_t Type of the cell.
     * @tparam plugin_t Type of the plugin.
     */
    template<typename cell_t, class plugin_t>
    class BodyDiffusion : public virtual BaseBody<plugin_t> {

    private:
        std::vector<double> consumptions; /**< Consumption of each molecule per step */
        int nbMolecules = 0; /**< Number of diffused molecules */

    public:

        /**
         * @brief Default constructor.
         */
        inline BodyDiffusion() : consumptions() {}

        /**
         * @brief Constructor with the number of existing molecules.
         * 
         * @param size Number of existing molecules.
         */
        inline explicit BodyDiffusion(int size) {
            initNbMolecules(size);
        }

        /**
         * @brief Initializes the quantities and consumptions.
         * 
         * @param n Number of existing molecules.
         */
        void initNbMolecules(int n) { 
            if(n > nbMolecules) {
                nbMolecules = n;
                consumptions.assign(n, 0.);
            }
        }

        /**
         * @brief Gets the quantity of a molecule at a specific position.
         * 
         * @param x X-coordinate.
         * @param y Y-coordinate.
         * @param mol Number of the molecule.
         * @return Quantity of the molecule.
         */
        double getQuantity(int x, int y, int mol) { 
            if(this->cellPlugin != nullptr) return getDiffusionGrid()->getMolecule(x, y, mol);
            else return 0.;
        }

        /**
         * @brief Gets the consumption of a molecule.
         * 
         * @param mol Number of the molecule.
         * @return Consumption of the molecule.
         */
        double getConsumption(int mol) const {
            if(this->cellPlugin != nullptr) return consumptions[getDiffusionGrid()->moleculesDict[mol]];
            else return consumptions[mol];
        }

        /**
         * @brief Gets the consumptions of all molecules.
         * @return Vector of consumptions.
         */
        inline std::vector<double> getConsumptions() const { return consumptions; }

        /**
         * @brief Sets the consumption of a molecule.
         * 
         * @param mol Number of the molecule.
         * @param value Consumption value.
         */
        inline void setConsumption(int mol, double value) {
            if(this->cellPlugin != nullptr) consumptions[getDiffusionGrid()->moleculesDict[mol]] = value;
            else consumptions[mol] = value;
        }

        /**
         * @brief Gets the diffusion grid.
         * 
         * @return Pointer to the DiffusionGrid.
         */
        inline DiffusionGrid* getDiffusionGrid() { return this->cellPlugin->pluginDiffusion.getDiffusionGrid(); }

        /**
         * @brief Hook called when the body is linked to the plugin.
         *
         * This hook is called when the body has access to the plugin, which happens when the cell is added to the world (w.addCell(c)).
         * This hook can be useful for setting up Body properties based on elements from the Plugin part.
         */
        void onCellPluginLinking() {
            auto grid = getDiffusionGrid();
            std::vector<double> consCopy(consumptions);
            for(int i = 0; i < consumptions.size(); ++i) consumptions[i] = consCopy[grid->moleculesDict[i]];
        }

    };

}

#endif //ONKO3D_3_0_BODYDIFFUSION_HPP
