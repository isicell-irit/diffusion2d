#ifndef ONKO3D_3_0_PLUGINDIFFUSION_HPP
#define ONKO3D_3_0_PLUGINDIFFUSION_HPP

/**
 * @file PluginDiffusion.hpp
 * @brief Defines the PluginDiffusion class for grid-based diffusion.
 */

#include <vector>
#include <mecacell/mecacell.h>
#include <math.h>
#include "DiffusionGrid.hpp"

/**
 * @namespace Diffusion2D
 * @brief Namespace for 2D diffusion-related classes and functions.
 */
namespace Diffusion2D {

    /**
     * @class PluginDiffusion
     * @brief Class for managing grid-based diffusion.
     * 
     * @tparam cell_t Type of the cell.
     */
    template<typename cell_t>
    class PluginDiffusion {

    private:
        DiffusionGrid grid; /**< Molecule grid */

    public:

        /**
         * @brief Default constructor.
         */
        inline PluginDiffusion() : grid() {}

        /**
         * @brief Constructor with grid dimensions.
         * 
         * @param w Width of the grid.
         * @param h Height of the grid.
         */
        inline PluginDiffusion(int w, int h) {
            grid.initGrid(w, h);
        }

        /**
         * @brief Constructor with grid size.
         * 
         * @param size Size of the square grid.
         */
        inline PluginDiffusion(int size) {
            grid.initGrid(size);
        }

        /**
         * @brief Gets the diffusion grid.
         * 
         * @return Pointer to the DiffusionGrid.
         */
        inline DiffusionGrid* getDiffusionGrid() { return &grid; }

        /**
         * @brief Adds a molecule to the grid.
         * 
         * @param id id of the molecule. if you use Enum, you can cast it to int.
         * @param m Molecule to be added.
         */
        inline void addMolecule(int id, Molecule m) { grid.addMolecule(id, m); }

        /**
         * @brief Initializes the square diffusion grid.
         * 
         * @param size Size of the grid.
         */
        inline void initDiffusionGrid(int size) { grid.initGrid(size); }

        /**
         * @brief Initializes the diffusion grid with dimensions.
         * 
         * @param w Width of the grid.
         * @param h Height of the grid.
         */
        inline void initDiffusionGrid(int w, int h) { grid.initGrid(w, h); }

        /**
         * @brief Sets the toroidal property on the x-axis.
         * 
         * @param b Boolean value for the toroidal property.
         */
        inline void setToreX(bool b) { grid.setToreX(b); }

        /**
         * @brief Sets the toroidal property on the y-axis.
         * 
         * @param b Boolean value for the toroidal property.
         */
        inline void setToreY(bool b) { grid.setToreY(b); }

        /**
         * @brief Sets the toroidal properties on both axes.
         * 
         * @param x Boolean value for the x-axis.
         * @param y Boolean value for the y-axis.
         */
        inline void setTore(bool x, bool y) { grid.setTore(x, y); }

        /**
         * @brief Pre-behavior update hook for MecaCell.
         *
         * Computes and updates the molecule quantity for each cell.
         *
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void preBehaviorUpdate(world_t *w) {
            grid.computeMolecules(w);
        }
    };
}

#endif //ONKO3D_3_0_PLUGINDIFFUSION_HPP