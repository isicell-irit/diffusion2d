# Diffusion2D

This folder contains the body and plugin classes that could be used to simulate diffusion of molecules in a 2D discrete environment using *MecaCell*.

It was tested on Debian 10.

## Prerequisites

- work on a 2D *MecaCell* simulation
- have a general body class that will inherit the **BodyDiffusion** class
- have a general plugin class or struct that will use the **PluginDiffusion** class

## How to use it

### Body

- include the folder in your *MecaCell* project
- import the **BodyDiffusion.hpp** file in the file containing your simulation general body class
- make your general body class inherit from the **BodyDiffusion** class
```cpp 
template <class cell_t> class CellBody : public BodyDiffusion 
```
- in your general body class constructor, add the following line to specify the number of molecules to diffuse
```cpp
explicit CellBody(cell_t *, MecaCell::Vec &pos = MecaCell::Vec::zero())
    : BodyDiffusion(n){}
```
OR 
- use the *initNbMolecule* method in your *Cell* class to initialize the number of molecules to diffuse
```cpp
this->getBody().initNbMolecules(n);
```
n corresponds to the number of different molecules you want to diffuse

### Plugin

- import the **PluginDiffusion.hpp** file in the file containing your simulation general plugin class
- add a new **PluginDiffusion** attribute to your plugin class
- in the onRegister method, add the new attribute 
```cpp 
template <typename world_t>  
void onRegister(world_t* w){      
w->registerPlugin(diffusionPlugin);  
} 
``` 
Your simulation will now be able to add and diffuse molecules in the environment and make your cells interact with them.

## How to calibrate your simulation

### Instantiating the plugin

In the general plugin class, you'll have to instantiate this plugin with the other ones.
The constructor needs 1 arguments : n, the side size of the square grid
```cpp 
PluginDiffusion diffusionPlugin = PluginDiffusion(w,h);
``` 
This calibration means that there is w x h positions in the diffusion grid.

### Setting the tore properties

By default, the diffusion grid is toric on both x and y axes.
To change the tore properties of the grid, you can set them in your *Scenario* class through the *World* like this :
```cpp
w.cellPlugin.diffusionPlugin.setTore(false,false);
```

### Adding new molecules in the simulation

You can add molecules and their characteristics in your simulation. You will have to do this through the *World* in your *Scenario* at initialization.
```cpp
Molecule oxygen(0.4, 37.5,0.);
Molecule carbon(0.6,0.,0.25);
w.cellPlugin.diffusionPlugin.addMolecule((int)MOLECULES::CARBON, carbon);
w.cellPlugin.diffusionPlugin.addMolecule((int)MOLECULES::OXYGEN, oxygen);
```
The first thing to do is to instantiate your molecules.
The constructor needs 3 parameters :
- diffusion coefficient between 0 and 1 corresponding to the proportion of the molecule that will stay in its current position and won't diffuse to the adjacent positions at each step
- default quantity in the environment
- default absorption of the environment at each step

Setting the default consumption with a negative value will make the molecule appear from nowhere

The indexes of each molecule will be in the same order as the order you added them.
In order to make it easier to access a molecule from its index, you can create an enum like this one :
```cpp
enum MOLECULES{
    OXYGEN = 0,
    CARBON = 1
};
```
It can prevent you from accessing the wrong molecule.

### Setting consumptions and accessing quantities from your cells

You can access and modify the quantity of every molecule from every cell thanks to the **BodyDiffusion** class.
```cpp
double oxygen = this->getBody().getQuantities()[MOLECULES::OXYGEN];
double carbon = this->getBody().getQuantities()[MOLECULES::CARBON];
```

You can do the same with the consumptions.
```cpp
this->getBody().setConsumption(MOLECULE::OXYGEN, 0.5634224);
this->getBody().setConsumption(MOLECULE::CARBON, -0.5);
```
A positive consumption will make the cell absorb the molecule while a negative one will make it produce the molecule.