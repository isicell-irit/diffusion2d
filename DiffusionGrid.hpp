#ifndef ONKO3D_3_0_DIFFUSIONGRID_HPP
#define ONKO3D_3_0_DIFFUSIONGRID_HPP

/**
 * @file DiffusionGrid.hpp
 * @brief Manages a 2D grid for molecule diffusion.
 *
 * Contains the DiffusionGrid class used in the 2D diffusion plugin.
 */

#include <vector>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <mecacell/utilities/utils.hpp>

/**
 * @namespace Diffusion2D
 * @brief Namespace for 2D diffusion-related classes and functions.
 */
namespace Diffusion2D {

    /**
     * @struct Molecule
     * @brief Represents a molecule with diffusion properties.
     */
    struct Molecule {
        double diffusionCoef; /**< Diffusion coefficient between 0 and 1 */
        double defaultQuantity; /**< Default quantity of the molecule in the environment */
        double defaultEvaporation; /**< Evaporation rate of the molecule per step */

        Molecule(double dc, double dq, double de)
        : diffusionCoef(dc), defaultQuantity(dq), defaultEvaporation(de) 
        {}
    };

    /**
     * @struct GridCell
     * @brief Represents a cell in the diffusion grid.
     */
    struct GridCell {
        std::vector<double> prevQuantities; /**< Previous quantities of each molecule */
        std::vector<double> quantities; /**< Current quantities of each molecule */
        std::vector<double> consumptions; /**< Local consumptions of each molecule */

        /**
         * @brief Default constructor.
         */
        inline GridCell() = default;

        /**
         * @brief Constructor with existing molecules.
         * @param m Vector of existing molecules in the simulation.
         */
        explicit GridCell(std::vector<Molecule> m) {
            int size = m.size();
            quantities.resize(size);
            prevQuantities.resize(size);
            consumptions.assign(size, 0.);
            for (int i = 0; i < size; ++i) {
                quantities[i] = m[i].defaultQuantity;
                prevQuantities[i] = m[i].defaultQuantity;
            }
        }
    };

    /**
     * @class DiffusionGrid
     * @brief Manages a grid of molecules for diffusion.
     */
    class DiffusionGrid {
    private:
        std::vector<std::vector<GridCell>> grid; /**< Matrix of grid cells */
        std::vector<Molecule> molecules; /**< Existing molecules in the simulation */

        bool toreX = true; /**< Toroidal property on the x-axis */
        bool toreY = true; /**< Toroidal property on the y-axis */

        int height = 10;
        int width = 10;

        /**
         * @brief Initializes cells for the next step.
         */
        void preStep() {
            for (int i = 0; i < grid.size(); ++i) {
                for (int j = 0; j < grid.size(); ++j) {
                    for(int m = 0; m < molecules.size(); ++m){
                        grid[i][j].prevQuantities[m] = grid[i][j].quantities[m];
                        grid[i][j].quantities[m] = 0.;
                        grid[i][j].consumptions[m] = 0.;
                    }
                }
            }
        }

        /**
         * @brief Applies diffusion to the molecule at position (x, y).
         * 
         * @param x X-coordinate.
         * @param y Y-coordinate.
         */
        void diffuse(int x, int y){
            GridCell &gc = grid[x][y];
            int nbNeighbors = 8;
            for (int i = -1; i <= 1; ++i) {
                for (int j = -1; j <= 1; ++j) {
                    if(i != 0 || j != 0){
                        int xi = x + i;
                        int yj = y + j;
                        if (toreX && toreY) { 
                            if(xi < 0) xi = width -1;
                            if(xi >= width) xi = 0;
                            if(yj < 0) yj = height -1;
                            if(yj >= height) yj = 0;
                            for(int m = 0; m < molecules.size(); ++m){
                                gc.quantities[m] += 1./8. * molecules[m].diffusionCoef * grid[xi][yj].prevQuantities[m];
                            }
                        }else if(toreX){
                            if(xi < 0) xi = width -1;
                            if(xi >= width) xi = 0;
                            if(yj >= 0 && yj < height){
                                for(int m = 0; m < molecules.size(); ++m){
                                    gc.quantities[m] += 1./8. * molecules[m].diffusionCoef * grid[xi][yj].prevQuantities[m];
                                }
                            }else nbNeighbors --;
                        }else if(toreY){
                            if(yj < 0) yj = height -1;
                            if(yj >= height) yj = 0;
                            if(xi >= 0 && xi < width){
                                for(int m = 0; m < molecules.size(); ++m){
                                    gc.quantities[m] += 1./8. * molecules[m].diffusionCoef * grid[xi][yj].prevQuantities[m];
                                }
                            }else nbNeighbors --;
                        }else{ 
                            if(xi >= 0 && xi < width && yj >= 0 && yj < height){
                                for(int m = 0; m < molecules.size(); ++m){
                                    gc.quantities[m] += 1./8. * molecules[m].diffusionCoef * grid[xi][yj].prevQuantities[m];
                                }
                            }
                            else nbNeighbors --;
                        }
                    }
                }
            }
            for(int m = 0; m < molecules.size(); ++m){
                gc.quantities[m] += gc.prevQuantities[m] * (1. - molecules[m].diffusionCoef) + (8-nbNeighbors)/8. *molecules[m].diffusionCoef*gc.prevQuantities[m] - molecules[m].defaultEvaporation - gc.consumptions[m];
                if (gc.quantities[m] < 0.) { //can't have a negative amount of molecule
                    gc.quantities[m] = 0.;
                }
            }
        }

        /**
         * @brief Updates the quantity of molecules for each cell.
         */
        void computeStep() {
            for (int i = 0; i < grid.size(); ++i) {
                for (int j = 0; j < grid.size(); ++j) { //for each cell in the grid
                    diffuse(i,j);
                }
            }
        }

        /**
         * @brief Updates the consumptions for each cell.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void updateConsumptions(world_t *w){
            for(auto cell : w->cells){
                int x = cell->getBody().get2DPosition().x();
                int y = cell->getBody().get2DPosition().y();
                for(int m = 0; m < molecules.size(); ++m){
                    grid[x][y].consumptions[m] += cell->getBody().getConsumptions()[m];
                }
            }
        }

    public:

        std::unordered_map<int,int> moleculesDict;

        DiffusionGrid() = default;

        /**
         * @brief Sets the toroidal property on the x-axis.
         * 
         * @param b Boolean value for the toroidal property.
         */
        inline void setToreX(bool b) { toreX = b; }

        /**
         * @brief Sets the toroidal property on the y-axis.
         * 
         * @param b Boolean value for the toroidal property.
         */
        inline void setToreY(bool b) { toreY = b; }

        /**
         * @brief Sets the toroidal properties on both axes.
         * 
         * @param x Boolean value for the x-axis.
         * @param y Boolean value for the y-axis.
         */
        inline void setTore(bool x, bool y) { toreX = x; toreY = y; }

        /**
         * @brief Gets the quantity of a molecule at a specific position.
         * 
         * @param x X-coordinate.
         * @param y Y-coordinate.
         * @param m Number of the molecule.
         * @return Quantity of the molecule.
         */
        inline double getMolecule(int x, int y, int m) { return(grid[x][y].quantities[moleculesDict[m]]); }

        /**
         * @brief Gets the width of the grid.
         * @return Width of the grid.
         */
        inline int getWidth() const { return width; }

        /**
         * @brief Gets the height of the grid.
         * @return Height of the grid.
         */
        inline int getHeight() const { return height; }

        /**
         * @brief Gets the grid.
         * @return Reference to the grid.
         */
        inline std::vector<std::vector<GridCell>> &getGrid() { return grid; }

        /**
         * @brief Gets the molecules in the grid.
         * @return Vector of molecules.
         */
        inline std::vector<Molecule> getMolecules() const { return molecules; }

        /**
         * @brief Adds a molecule to the grid.
         * 
         * @param id id of the molecule. if you use Enum, you can cast it to int.
         * @param m Molecule to be added.
         */
        void addMolecule(int id, Molecule m) {
            moleculesDict[id] = (int) molecules.size(); 
            molecules.push_back(m); 
        }

        /**
         * @brief Initializes the grid with dimensions.
         * 
         * @param w Width of the grid.
         * @param h Height of the grid.
         */
        void initGrid(int w, int h){
            grid.resize(w);
            for(int i = 0; i < w; ++i){
                grid[i].resize(h);
                for(int j = 0; j < h; ++j){
                    grid[i][j] = GridCell(molecules);
                }
            }
            height = h;
            width = w;
        }

        /**
         * @brief Initializes the square grid.
         * 
         * @param size Size of the grid.
         */
        void initGrid(int size) { initGrid(size, size); }

        /**
         * @brief Computes the quantities of molecules in the grid.
         * 
         * @tparam world_t Type of the world.
         * @param w Pointer to the world.
         */
        template<typename world_t>
        void computeMolecules(world_t *w) {
            preStep();
            updateConsumptions(w);
            computeStep();
        }
    };
}

#endif //ONKO3D_3_0_DIFFUSIONGRID_HPP
